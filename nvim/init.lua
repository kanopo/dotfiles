-- startup packer(pachage manager)
require 'user.packer'

-- colorscheme
require 'user.plugins.impatient'
require 'user.plugins.colorscheme'
require 'user.plugins.lualine'
require 'user.plugins.nvim-tree'
require 'user.plugins.telescope'
require 'user.plugins.nvim-cmp'
require 'user.plugins.lsp'
require 'user.plugins.treesitter'
require 'user.plugins.alpha'
require 'user.plugins.bufferline'
require 'user.plugins.gitsigns'

-- user options
require 'user.core.options'

require 'user.core.keymaps'
